public class Position {

    private String word;
    private int startColumnIndex;
    private int startLineIndex;
    private int endColumnIndex;
    private int endLineIndex;

    public void setWord(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setStartColumnIndex(int startColumnIndex) {
        this.startColumnIndex = startColumnIndex;
    }

    public int getStartColumnIndex() {
        return startColumnIndex;
    }

    public void setStartLineIndex(int startLineIndex) {
        this.startLineIndex = startLineIndex;
    }

    public int getStartLineIndex() {
        return startLineIndex;
    }

    public void setEndColumnIndex(int endColumnIndex) {
        this.endColumnIndex = endColumnIndex;
    }

    public int getEndColumnIndex() {
        return endColumnIndex;
    }

    public void setEndLineIndex(int endLineIndex) {
        this.endLineIndex = endLineIndex;
    }

    public int getEndLineIndex() {
        return endLineIndex;
    }

    @Override
    public String toString() {
        return "word[" + word + "] from[" + startLineIndex + "][" + startColumnIndex + "] to[" + endLineIndex + "]["
                + endColumnIndex + "]";
    }

}