import java.util.*;

public class MainProgram {

    private static final String CHARACTERS[] = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
    private static final String WORDS[] = { "cd", "dvd", "vhs" };
    private static Random random = new Random();

    public static void main(String[] args) {
        if (args.length <= 0) {
            throw new IllegalArgumentException("Application requires the matrix size as first argument");
        }
        long startTime = System.currentTimeMillis();
        int size = Integer.valueOf(args[0]);
        String[][] matrix = createMatrix(size);
        printMatrix(matrix);
        searchWords(matrix, Arrays.asList(WORDS));
        long endTime = System.currentTimeMillis();
        System.out.println("Time elapsed: " + (endTime - startTime) + "ms");
    }

    private static String[][] createMatrix(int size) {
        String[][] matrix = new String[size][size];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                int number = random.nextInt(CHARACTERS.length);
                matrix[i][j] = CHARACTERS[number];
            }
        }
        return matrix;
    }

    private static void printMatrix(String[][] matrix) {
        printMatrix(matrix, false);
    }

    private static void printMatrix(String[][] matrix, boolean showIndex) {
        System.out.println("--- Matrix ---");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (showIndex) {
                    System.out.print(String.format("%s[%s][%s] ", matrix[i][j], i, j));
                } else {
                    System.out.print(matrix[i][j] + " ");
                }
            }
            System.out.println("");
        }
        System.out.println("");
    }

    private static void searchWords(String[][] matrix, List<String> words) {
        for (String word : words) {
            searchHorizontal(matrix, word);
            searchHorizontalInverted(matrix, word);
            searchVertical(matrix, word);
            searchVerticalInverted(matrix, word);
        }
    }

    private static void searchHorizontal(String[][] matrix, String word) {
        List<Position> positions = search(matrix, word);
        System.out.println("--- Horizontal ---");
        if (positions.isEmpty()) {
            System.out.println("Nothing found!");
        } else {
            for (Position position : positions) {
                System.out.println(position);
            }
        }
        System.out.println("");
    }

    private static void searchHorizontalInverted(String[][] matrix, String word) {
        List<Position> positions = search(matrix, new StringBuilder(word).reverse().toString());
        System.out.println("--- Horizontal Inverted ---");
        if (positions.isEmpty()) {
            System.out.println("Nothing found!");
        } else {
            for (int i = 0; i < positions.size(); i++) {
                Position position = positions.get(i);
                position.setWord(word);
                System.out.println(position);
            }
        }
        System.out.println("");
    }

    private static void searchVertical(String[][] matrix, String word) {
        String[][] transposedMatrix = transposeMatrix(matrix);
        List<Position> positions = search(transposedMatrix, word);
        System.out.println("--- Vertical ---");
        if (positions.isEmpty()) {
            System.out.println("Nothing found!");
        } else {
            for (int i = 0; i < positions.size(); i++) {
                Position position = positions.get(i);

                int aux = position.getStartLineIndex();
                position.setStartLineIndex(position.getStartColumnIndex());
                position.setStartColumnIndex(aux);

                aux = position.getEndLineIndex();
                position.setEndLineIndex(position.getEndColumnIndex());
                position.setEndColumnIndex(aux);

                System.out.println(position);
            }
        }
        System.out.println("");
    }

    private static void searchVerticalInverted(String[][] matrix, String word) {
        String[][] transposedMatrix = transposeMatrix(matrix);
        List<Position> positions = search(transposedMatrix, new StringBuilder(word).reverse().toString());
        System.out.println("--- Vertical Inverted ---");
        if (positions.isEmpty()) {
            System.out.println("Nothing found!");
        } else {
            for (int i = 0; i < positions.size(); i++) {
                Position position = positions.get(i);
                position.setWord(word);

                int aux = position.getStartLineIndex();
                position.setStartLineIndex(position.getStartColumnIndex());
                position.setStartColumnIndex(aux);

                aux = position.getEndLineIndex();
                position.setEndLineIndex(position.getEndColumnIndex());
                position.setEndColumnIndex(aux);

                System.out.println(position);
            }
        }
        System.out.println("");
    }

    private static List<Position> search(String[][] matrix, String word) {
        String line;
        StringBuilder sb;
        List<Position> positions = new ArrayList<Position>();
        for (int i = 0; i < matrix.length; i++) {
            sb = new StringBuilder();
            for (int j = 0; j < matrix[i].length; j++) {
                sb.append(matrix[i][j]);
            }

            line = sb.toString();
            int index = -1;
            do {
                index = line.indexOf(word, index + 1);
                if (index != -1) {
                    Position position = new Position();
                    position.setWord(word);
                    position.setStartLineIndex(i);
                    position.setStartColumnIndex(index);
                    position.setEndLineIndex(i);
                    position.setEndColumnIndex((index + word.length() - 1));
                    positions.add(position);
                }
            } while (index != -1);
        }
        return positions;
    }

    private static String[][] transposeMatrix(String[][] matrix) {
        String[][] transposedMatrix = new String[matrix[0].length][matrix.length];
        for (int i = 0; i < transposedMatrix.length; i++) {
            for (int j = 0; j < transposedMatrix.length; j++) {
                transposedMatrix[i][j] = matrix[j][i];
            }
        }
        return transposedMatrix;
    }

}